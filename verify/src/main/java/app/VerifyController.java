package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import app.exception.BadRequestException;
import java.util.List;
import java.util.Arrays;
import java.math.BigDecimal;
import java.util.regex.Pattern;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    private List<String> illegalSequences =
        Arrays.asList("&", "?", "%26", "%3f", "%3F");

    private Pattern amountPattern =
        Pattern.compile("-?[0-9]+(\\.[0-9]*)?");

    private String validateParameter(String value) {
        if (value == null) {
            throw new IllegalArgumentException();
        }
        for (String illegalSequence : this.illegalSequences) {
            if (value.contains(illegalSequence)) {
                throw new IllegalArgumentException();
            }
        }
        return value;
    }

    private BigDecimal validateAmount(String amount) {
        if (amount.length() <= 0 || amount.length() > 15) {
            throw new IllegalArgumentException();
        }
        if (amountPattern.matcher(amount).matches()) {
            BigDecimal decimal = new BigDecimal(amount);
            if (decimal.signum() < 0) {
                throw new IllegalArgumentException();
            }
            return decimal;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        try {
            String action = validateParameter(request.getParameter("action"));
            BigDecimal amount = validateAmount(validateParameter(request.getParameter("amount")));
            if (action.equals("transfer")) {
                System.out.println("Verify Controller: Going to transfer $"+amount);
                RestTemplate restTemplate = new RestTemplate();
                String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
                ResponseEntity<String> response 
                    = restTemplate.getForEntity(
                            fakePaymentUrl + "?action={action}&amount={amount}",
                            String.class,
                            action,
                            amount);
                return response.getBody();
            } else if (action.equals("withdraw")) {
                return "Verify Controller: Sorry, you can only make transfer";
            } else {
                return "Verify Controller: You must specify action: transfer or withdraw";
            }
        } catch(RuntimeException ex) {
            throw new BadRequestException();
        }
    }

}
